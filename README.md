# YAJDSP

A digital filter library and visualization tools written in JAVA.  Filters include highpass, 
lowpass, bandpass and bandstop as Butterworth, Bessel Chebyshev Type I/II, and Elyptic.

This code mostly comes from Bernd Porr and the contributors he recognizes.  I've added Java GUI tools around the edges,
refactored the package names, and updated the build system.

IIR1 library [https://github.com/berndporr/iir1]
IIRJ library [https://github.com/berndporr/iirj]
DSPFilters [https://github.com/vinniefalco/DSPFilters].

Design Assistance [https://github.com/mmitch/apsynth.git]

![alt tag](filtertest.png)

## Usage

`import com.softhorizons.yajdsp.*;`

### Constructor

  `Butterworth butterworth = new Butterworth();`

### Initialisation
1. Bandstop

   `butterworth.bandStop(order,Samplingfreq,Center freq,Width in frequ);`

2. Bandpass

   `butterworth.bandPass(order,Samplingfreq,Center freq,Width in frequ);`

3. Lowpass

   `butterworth.lowPass(order,Samplingfreq,Cutoff frequ);`

4. Highpass

   `butterworth.highPass(order,Samplingfreq,Cutoff frequ);`

### Filtering
Sample by sample for realtime processing:

```
v = butterworth.filter(v)
```

## Coding examples
See the `*Test.java` files for complete examples
for all filter types. Run them with `mvn test`. These test programs
write the different impulse responses of the filters to text files.

## Installation
Just run: `mvn install` to add it to your local maven respository or
just point your project to Maven Central:

## Maven central
[http://search.maven.org/#artifactdetails%7Ccom.softhorizons%7Cyajdsp]

## Android Studio
```
dependencies {
    compile group: 'com.softhorizons', name:'yajdsp', version: '0.1'
}
```

## Documentation
* `mvn javadoc:javadoc` generates the JavaDocs
* `mvn site` generates the web pages containing the documentation
under `target/site` describing all commands in detail.

## Testing
`mvn test` creates impulse responses in the subdirectories
for the different filters: `target/surefire-reports`.

To see the frequency responses install octave, copy the script
'src/test/resources/filtertest.m'
in these subdirectories and run it from there. You should see the
different frequency reponses for high/low/stop/bandpass. You can try
out different filter parameters by modifiing the test
scripts and re-run 'mvn test'.

The script DetectorTest uses a bandpass filter to detect the
heartbeats of an ECG recording faking a matched filter which could
be also seen as a 1st approximation of a wavelet. The heartrate is
stored in hr.txt.



Have fun

/bbaker
