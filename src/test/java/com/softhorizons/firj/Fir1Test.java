package com.softhorizons.firj;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;

public class Fir1Test {

	public static void main(String[] args) throws IOException {
		
		File root = new File("c:/opt/git/github/fir1/demo"); 
		File coeffFile = new File(root, "coefficients.dat");
		
		File datFile = new File(root, "impulse2.dat");
		
		
		MovingAverage fir = new MovingAverage(coeffFile);
		int taps = fir.getTaps();
		
		PrintStream writer = new PrintStream(datFile);

		for( int i =0; i < 2*taps; i++ ) {
			double a = 0;
			if( i == 10 ) a = 1;
			double b = fir.filter(a);
			
			writer.printf( "%7.6f%n", b );
		}

		writer.close();
	}

}
