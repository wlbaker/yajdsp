/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *  Copyright (c) 2009 by Vinnie Falco
 *  Copyright (c) 2016 by Bernd Porr
 */

package uk.me.berndporr.api;

import org.apache.commons.math3.complex.Complex;

/**
 * PoleZeroPair with gain factor
 */
public class BiquadPoleState extends PoleZeroPair {

	public BiquadPoleState(Biquad s) {
		final double a0 = s.getA0();
		final double a1 = s.getA1();
		final double a2 = s.getA2();
		final double b0 = s.getB0();
		final double b1 = s.getB1();
		final double b2 = s.getB2();

		if (a2 == 0 && b2 == 0) {
			// single pole
			poles.first = new Complex(-a1, 0);
			zeros.first = new Complex(-b0 / b1, 0);
			poles.second = new Complex(0, 0);
			zeros.second = new Complex(0, 0);
		} else {
			{
				final Complex c = (new Complex(a1 * a1 - 4 * a0 * a2, 0)).sqrt();
				double d = 2. * a0;
				poles.first = new Complex(-(a1 + c.getReal()) / d, -c.getImaginary() / d);
				poles.second = new Complex((c.getReal() - a1) / d, c.getImaginary() / d);
				assert (!poles.is_nan());
			}

			{
				final Complex c = (new Complex(b1 * b1 - 4 * b0 * b2, 0)).sqrt();
				double d = 2. * b0;
				zeros.first = new Complex(-(b1 + c.getReal()) / d, -c.getImaginary() / d);
				zeros.second = new Complex((c.getReal() - b1) / d, c.getImaginary() / d);
				assert (!zeros.is_nan());
			}
		}

		gain = b0 / a0;
	}

	public BiquadPoleState(Complex p, Complex z) {
		super(p, z);
	}

	public BiquadPoleState(Complex p1, Complex z1, Complex p2, Complex z2) {
		super(p1, z1, p2, z2);
	}

	double gain;

}
