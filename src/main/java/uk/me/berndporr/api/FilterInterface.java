package uk.me.berndporr.api;

import java.util.Collection;

import org.apache.commons.math3.complex.Complex;

public interface FilterInterface {
	public double filter(double in);

	public void setDescription(String string);
	public String getDescription();

	public void setSample_rate(double doubleValue);
	public double getSample_rate();
	
	public Complex response(double f);

	public void reset();

	public Collection<? extends PoleZeroPair> getPoleZeros();
	
	public String getName();
	public void setName( String name );
}
