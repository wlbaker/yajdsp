package com.softhorizons.yajdsps;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.math3.complex.Complex;

public class PhaseCanvas extends BaseCanvas {

	private final static float MARGIN = 0.1f;

	public PhaseCanvas() {
		setY_lab("Phase Response (Degree)");
		setX_lab("Frequency (Hz)");
		setYAxisTicks( new double[] { -200, -100, 0, 100, 200} );
		setYMax( -200 );
		setYMin( 200 );
		
		setXMin( 0.0 );
		setXMax( 0.5 );
		// setXAxisTicks(new double[] { 0.0, 0.1, 0.2, 0.3, 0.4, 0.5 });

	}
	
	boolean drawPhaseLine (Graphics2D g, int degrees, boolean drawLabel)
	{
	  boolean onScreen = true;

	  Rectangle bounds = getBounds ();
	  Rectangle r = bounds;
	  final int y = yToScreen ( (float)degrees );

	  // if (y >= r.getY() && y < r.getBottom())
	  // {
		g.drawLine(0, y, r.width, y);

	    if (drawLabel)
	    {
	    	String s = "" + degrees;
	      if (degrees >= 0)
	        g.drawString(s, 6, y+14 );
	      else
	        g.drawString(s, 6, y-4 );
	    }
//	  }  else  {
//	    onScreen = false;
//	  }
	    
	    System.out.println( "yold=" + degrees + " ynew=" + y );

	  return onScreen;
	}

//	@Override
//	public void paint(Graphics g) {
//		super.paint(g);
//
//		Graphics2D g2 = (Graphics2D)g;
//		
//		g2.setColor(Color.black);
//		drawPhaseLine(g2, 0, false);
//		drawPhaseLine(g2, 90, true);
//		drawPhaseLine(g2, -90, true);
//
//		AffineTransform t00 = g2.getTransform();
//		AffineTransform t0 = g2.getTransform();
//		AffineTransform t1 = calcTransform();
//		
//		System.out.println("trans=" + t1);
//		t0.concatenate(t1);
//		
//		g2.setTransform( t0 );
//		g2.setColor(Color.BLUE);
//		g2.draw(path);
//		
//		g2.setTransform( t00 );
//	}
//

	@Override
	public void update() {

		path.reset();

		if (filter != null) {
			isDefined = true;

			// Rectangle2D bounds = getLocalBounds ();
			Rectangle bounds = this.getBounds();
			Rectangle2D r = new Rectangle2D.Float(0, 0, (float) bounds.getWidth() - 4, (float) bounds.getHeight() - 4);

			// AffineTransform t;
			// PathIterator itt = path.getPathIterator( t );

			int iwidth = (int) r.getWidth();
			for (int xi = 0; xi < iwidth; xi++) {
				double f = xi / r.getWidth(); // [0..1)

				Complex c = filter.response(f / 2);
				double y = 180 * c.getArgument() / Math.PI; // radians to degrees.

				if (!isNaN(y)) {
					if (xi == 0)
						path.moveTo(xi, y);
					else
						path.lineTo(xi, y);
				} else {
					path.reset();
					isDefined = false;
					break;
				}
			}

			// if (m_isDefined)
			// path.startNewSubPath (0, 0);
		}

		repaint();
	}

	protected AffineTransform calcTransform() {
		Rectangle bounds = this.getBounds();
		Rectangle2D r = new Rectangle2D.Float(0, 0, (float) bounds.getWidth() - 4, (float) bounds.getHeight() - 4);

		double x_scale = 1;
		double y_scale = r.getHeight() / (yMax - yMin);
		double tx = 0;
		double ty = r.getHeight() / 2;

		AffineTransform t = new AffineTransform(x_scale, 0, 0, y_scale, tx, ty);
		return t;
	}

}
