package com.softhorizons.yajdsps;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import org.apache.commons.math3.complex.Complex;

public class GroupCanvas extends BaseCanvas {

	private static final float MARGIN = 0.1f;

	public GroupCanvas() {
		
		setY_lab("Group Delay (\u00B5S)");
		setX_lab("Frequency (Hz)");
		setYAxisTicks( new double[] { -1, 0, 1} );
	}

	boolean drawGroupDelayLine(Graphics2D g, float seconds, boolean drawLabel) {
		boolean onScreen = true;

		Rectangle2D bounds = getBounds();
		Rectangle2D r = bounds;

		final int y = yToScreen(seconds);

		// if (y >= r.getY() && y < r.getBottom()) {
		// g.fillRect (r.getX(), y, r.getWidth(), 1);
		g.drawLine(0, y, (int) r.getWidth(), y);

		if (drawLabel) {
			String s = "" + seconds;
			if (seconds >= 0)
				g.drawString(s, (int) r.getX() + 6, y - 2);
			else
				g.drawString(s, (int) r.getX() + 6, y + 2);
		}
//	  }  else {
//	    onScreen = false;
//	  }

		return onScreen;
	}


	@Override
	public void update() {

		isDefined = false;
		path.reset();

		if (filter != null) {
			isDefined = true;

			// Rectangle2D bounds = getLocalBounds ();
			Rectangle bounds = this.getBounds();
			Rectangle2D r = new Rectangle2D.Float(0, 0, (float) bounds.getWidth() - 4, (float) bounds.getHeight() - 4);

			// AffineTransform t;
			// PathIterator itt = path.getPathIterator( t );

			int iwidth = (int) r.getWidth();
			float fwidth = (float) r.getWidth();
			
			float PI = (float) (Math.PI);
			for (int xi = 0; xi < iwidth; xi++) {
				float x = xi / fwidth; // [0..1)
				float w = PI * x/2;
				
			    Complex c = filter.response(w);
				float y = - (float) (c.abs()/w);

				if (!isNaN(y)) {
					if (xi == 0)
						path.moveTo(xi, y);
					else
						path.lineTo(xi, y);
				} else {
					path.reset();
					isDefined = false;
					break;
				}
			}

			// if (m_isDefined)
			// path.startNewSubPath (0, 0);
		}

		repaint();
	}

	protected AffineTransform calcTransform() {
		Rectangle bounds = this.getBounds();
		Rectangle2D r = new Rectangle2D.Float(0, 0, (float) bounds.getWidth() - 4, (float) bounds.getHeight() - 4);

		double x_scale = 1.0;
		
		// y --> [-3,3]
		double y_scale = -(float) r.getHeight() / (2f * 3);
		double tx = 0;
		double ty = r.getHeight() / 2 + 4;

		AffineTransform t = new AffineTransform(x_scale, 0, 0, y_scale, tx, ty);
		return t;
	}

}
