package com.softhorizons.yajdsps;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.awt.Rectangle;

import javax.swing.border.LineBorder;

import org.apache.commons.math3.complex.Complex;
import org.sittingbull.gt.util.NiceStepSizeGenerator;
import org.sittingbull.gt.util.XWilkinson;

import uk.me.berndporr.api.Cascade;
import uk.me.berndporr.iirj.Butterworth;

public class MagnitudeCanvas extends BaseCanvas {

	private static final float MARGIN = 0.05f;
	// private double m_scale_y;

	public MagnitudeCanvas() {
		setBackground(Color.WHITE);
		setBorder(new LineBorder(Color.BLACK));

		// title = "Magnitude";
		update();

		setY_lab("Magnitude Response (dB)");
		setX_lab("Sample Frequency Fs (Hz)");
		setXAxisType(BaseCanvas.AXIS_LINEAR); // LOGARITHMIC);
		setYAxisType(BaseCanvas.AXIS_LINEAR); // LOGARITHMIC);
		setYAxisTicks(new double[] { -140, -120.0, -100, -80, -60, -40, -20, 0, 20 });

		setYMax(-140);
		setYMin(20);

		// setXAxisTicks(new double[] { 0.0, 0.1, 0.2, 0.3, 0.4, 0.5 });
		setXMin(0.0);
		setXMax(0.5);

	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		Graphics2D g2 = (Graphics2D) g;

		/*
		 * AffineTransform t0 = g2.getTransform(); AffineTransform t1 =
		 * this.calcTransform();
		 * 
		 * System.out.println(title + " path drawn t0: " + t0); System.out.println(title
		 * + "            t1: " + t1); t0.concatenate(t1); System.out.println(title +
		 * "            tx: " + t0);
		 * 
		 * g2.setColor(Color.red); g2.setTransform(t0); // g2.draw(path);
		 */

		/*
		 * AffineTransform t = calcTransform();
		 * 
		 * { int y = yToScreen (0);
		 * 
		 * g.setColour (m_cAxis); g.fillRect (getLocalBounds().getX() + 1, y,
		 * getLocalBounds().getWidth() - 2, 1);
		 * 
		 * g.setColour (m_cText); drawText (g, Point<int> (6, y-2), "0"); }
		 * 
		 * { int y = yToScreen (1);
		 * 
		 * g.setColour (m_cAxis); g.fillRect (getLocalBounds().getX() + 1, y,
		 * getLocalBounds().getWidth() - 2, 1);
		 * 
		 * g.setColour (m_cText); drawText (g, Point<int> (6, y+2), "1",
		 * Justification::topLeft); }
		 * 
		 * // path g.setColour (Colors::blue); g.strokePath (m_path, 1, t);
		 */
	}

	@Override
	public void update() {

		isDefined = false;
		path.reset();

		if (filter != null) {
			isDefined = true;

			// Rectangle2D bounds = getLocalBounds ();
			Rectangle bounds = this.getBounds();
			Rectangle2D r = new Rectangle2D.Float(0, 0, (float) bounds.getWidth() - 4, (float) bounds.getHeight() - 4);

			// AffineTransform t;
			// PathIterator itt = path.getPathIterator( t );

			int iwidth = (int) r.getWidth();
			float fwidth = (float) r.getWidth();

			// float minExponent = 1.0f;
			// float maxExponent = 3.0f;
			// float dExponent = (maxExponent - minExponent) / iwidth;
			for (int xi = 0; xi < iwidth; xi++) {
				// float f = (float) Math.pow(10.0, minExponent + xi * dExponent );
				float xpos = xi / fwidth; // [0..1)
				float f = xpos; // xToF (x);

				Complex c = filter.response(f / 2.f);
				double y =  10.0 * Math.log(c.abs()); 
				
				if (!isNaN(y)) {
					if (xi == 0)
						path.moveTo(xi, y);
					else
						path.lineTo(xi, y);
				} else {
					path.reset();
					isDefined = false;
					break;
				}
			}

			// if (m_isDefined)
			// path.startNewSubPath (0, 0);
		}

		repaint();
	}

	protected AffineTransform calcTransform() {
		// Rectangle bounds = this.getBounds();
		Rectangle2D r = new Rectangle2D.Float(leftMargin, topMargin, (float) graphWidth, (float) graphHeight );

		// not dynamic
		double y_scale = -graphHeight / Math.abs(yMax - yMin);

		double x_scale = 1;
		double tx = 0;
		double ty = -yMin * y_scale + topMargin;

		AffineTransform t = new AffineTransform(x_scale, 0, 0, y_scale, tx, ty);
		return t;
	}

}
