package com.softhorizons.yajdsps;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.Vector;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import uk.me.berndporr.api.PoleZeroPair;

public class PoleZeroCanvas extends BaseCanvas {

	double m_max = 1;
	Color cAxis = Color.red;

	Vector<PoleZeroPair> vpz = new Vector<PoleZeroPair>();

	public PoleZeroCanvas() {
		title = "Pole/Zero";

		setPlotType(BaseCanvas.PT_UNIT_CIRCLE);
		setY_lab("Im(Z)");
		setX_lab("Re(Z)");
		setXAxisTicks(new double[] { -1.25, -1.0, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1.0, 1.25 });
		setYAxisTicks(new double[] { -1.25, -1.0, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1.0, 1.25 });

		setYMin(1.25);
		setYMax(-1.25);

		setXMin(-1.25);
		setXMax(1.25);
	}

	@Override
	public void update() {
		vpz.clear();

		if (filter != null) {
			Collection<? extends PoleZeroPair> poles = filter.getPoleZeros();
			if (poles != null) {
				vpz.addAll(filter.getPoleZeros());
			}
		}

		repaint();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g); // handles backgroun and border

		int x1 = leftMargin;
		int y1 = topMargin;
		int x2 = leftMargin + graphWidth;
		int y2 = topMargin + graphHeight;

		Graphics2D g2 = (Graphics2D) g;
		Color cPole = new Color(0xd0ff0000);
		Color cZero = new Color(0xd02020ff);

		Rectangle bounds = getBounds(); // getLocalBounds();
		int size = getXPos(1) - getXPos(0); // Math.min(x2-x1, y2-y1) / 2;
		// Rectangle bounds = new Rectangle(x1, y1, size, size);

		// scale the graph down if the pole/zeroes lie outside the unit disc
		/*
		 * AffineTransform t = new AffineTransform(); // identity
		 * 
		 * { float margin = 0.2f; if (m_max > 1 + margin) { t.scale((float) (1 / (m_max
		 * - margin)), (float) (1 / (m_max - margin))); } } t.scale(size, -size);
		 * t.translate((float) (bounds.getCenterX()), (float) (bounds.getCenterY()));
		 */

		AffineTransform t00 = g2.getTransform();
		AffineTransform t0 = g2.getTransform();
		AffineTransform t1 = new AffineTransform();
		t1.translate(getXPos(0), getYPos(0));
		t1.scale(size, -size);

		t0.concatenate(t1);

		g2.setTransform(t0);

		g.setColor(cAxis);
		g2.setStroke(new BasicStroke(0.15f / (float) size)); // 0.01f) );
		{
			// g2.drawOval( 0, 0, 1, 1);
			Ellipse2D.Double circle = new Ellipse2D.Double(-1, -1, 2, 2);
			g2.draw(circle);
		}

		// final float r = 0.02f;
		final int r = 3;

		g2.setTransform(t00);
		for (PoleZeroPair pzp : vpz) {

			if (!pzp.is_nan()) {
				Point2D p = new Point2D.Float((float) (pzp.poles.first.getReal()),
						(float) (pzp.poles.first.getImaginary()));
				t1.transform(p, p);
				g2.setColor(cPole);
				g2.drawLine((int) p.getX() - r, (int) p.getY() - r, (int) p.getX() + r, (int) p.getY() + r);
				g2.drawLine((int) p.getX() + r, (int) p.getY() - r, (int) p.getX() - r, (int) p.getY() + r);
			}

			{
				Point2D p = new Point2D.Float((float) pzp.zeros.first.getReal(),
						(float) pzp.zeros.first.getImaginary());
				t1.transform(p, p);
				g2.setColor(cZero);
				g2.drawOval((int) p.getX() - r, (int) p.getY() - r, 2 * r, 2 * r);
			}

			if (!pzp.isSinglePole()) {
				{
					Point2D p = new Point2D.Float((float) pzp.poles.second.getReal(),
							(float) pzp.poles.second.getImaginary());
					t1.transform(p, p);
					g2.setColor(cPole);
					g2.drawLine((int) (p.getX() - r), (int) (p.getY() - r), (int) (p.getX() + r), (int) (p.getY() + r));
					g2.drawLine((int) p.getX() + r, (int) p.getY() - r, (int) p.getX() - r, (int) p.getY() + r);
				}

				{
					Point2D p = new Point2D.Float((float) pzp.zeros.second.getReal(),
							(float) pzp.zeros.second.getImaginary());
					t1.transform(p, p);
					g2.setColor(cZero);
					g2.drawOval((int) p.getX() - r, (int) p.getY() - r, 2 * r, 2 * r);
				}
			}
		}
		g2.setTransform(t00);

	}

}
