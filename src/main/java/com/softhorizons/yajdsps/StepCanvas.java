package com.softhorizons.yajdsps;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import org.apache.commons.math3.complex.Complex;

public class StepCanvas extends BaseCanvas {

	private double sc_ymax;
	private double sc_ymin;

	public StepCanvas() {
		setY_lab("Impulse and Step Response");
		setYAxisTicks(new double[] { -0.25, -0.2, -0.15, -0.1, -0.05, 0, 0.05, 0.10, 0.15, 0.20 });
		setYMin(0.20);
		setYMax(-0.25);

		setX_lab("Time (ms)");
		setXAxisTicks(new double[] { 0, 40, 80, 120, 160, 200, 240, 280 });
		setXMin(0);
		setXMax(280);
	}

	@Override
	public void update() {

		isDefined = false;
		path.reset();
		path2.reset();

		sc_ymax = 0.01;
		sc_ymin = -0.01;

		if (filter != null) {
			filter.reset();

			Rectangle bounds = this.getBounds();
			Rectangle2D r = new Rectangle2D.Float(0, 0, (float) bounds.getWidth() - 4, (float) bounds.getHeight() - 4);

			int numSamples = 250;

			// # if 1
			float[] impulse = new float[numSamples];
			impulse[0] = 1;
			// impulse[1] = -1;
			isDefined = graphResponse(path2, impulse, Color.RED);  // impulse

			if (isDefined) {
				// // Heaviside step function
				impulse = new float[numSamples];
				impulse[0] = 0;
				impulse[1] = 0.5f;
				impulse[2] = 1;
				for (int i = 3; i < numSamples; i++) {
					impulse[i] = 1;
				}
				isDefined = graphResponse(path, impulse, Color.BLUE);  // step
			}
			
			this.setYMax( sc_ymin * 1.1 );  // inverted graphs
			this.setYMin( sc_ymax * 1.1 );  // inverted graphs

		}

		repaint();
	}

	private boolean graphResponse(Path2D path, float[] impulse, Color color) {
		int numSamples = impulse.length;
		
		for (int i = 0; i < numSamples; i++) {
			impulse[i] = (float) filter.filter(impulse[i]);
		}
		// filter.process(impulse);

		isDefined = true;

		int iwidth = numSamples - 2; // (int) r.getWidth();
		//double fwidth = (float) r.getWidth();

		for (int xi = 0; xi < iwidth; xi++) {
			//double x = xi * numSamples / (2 * fwidth);
			double x = xi * (graphWidth / (double)numSamples);
			//double t = x - Math.floor(x);
			float y0 = impulse[(int) xi];
			//float y1 = impulse[1 + (int) xi];
			//double y = y0 + t * (y1 - y0);
			double y = y0;

			if (!isNaN(y)) {
				// x /= numSamples;
				if (xi == 0)
					path.moveTo(x, y);
				else
					path.lineTo(x, y);

				//Shape s = new Ellipse2D.Double(x, y, 0.03, 0.03);
				//path.append(s, false);

				// System.out.println( color + " step " + xi + ": " + y);
				sc_ymax = Math.max(sc_ymax, y);
				sc_ymin = Math.min(sc_ymin, y);
			} else {
				path.reset();
				isDefined = false;
				break;
			}
		}

		return isDefined;
	}

	protected AffineTransform calcTransform() {
		Rectangle2D r = new Rectangle2D.Float(leftMargin, topMargin, (float)graphWidth, (float)graphHeight);

		double x_scale = 1.0;
		double y_scale = r.getHeight() / (yMax - yMin);
		double tx = 0;
		double ty = getYPos(0) + topMargin/2;  // FIXME: getYPos() already add topMargin...what is happening?

		AffineTransform t = new AffineTransform(x_scale, 0, 0, y_scale, tx, ty);
		return t;
	}

}
