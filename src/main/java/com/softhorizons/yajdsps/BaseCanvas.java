package com.softhorizons.yajdsps;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import org.sittingbull.gt.util.Label;
import org.sittingbull.gt.util.NiceStepSizeGenerator;
import org.sittingbull.gt.util.XWilkinson;

import lombok.Getter;
import lombok.Setter;
import uk.me.berndporr.api.Cascade;
import uk.me.berndporr.api.DirectFormAbstract;
import uk.me.berndporr.api.FilterInterface;
import uk.me.berndporr.iirj.Butterworth;
import uk.me.berndporr.iirj.ChebyshevI;

public class BaseCanvas extends JPanel implements ComponentListener, PropertyChangeListener {

	public static final int AXIS_LINEAR = 0;
	public static final int AXIS_LOGARITHMIC = 1;
	public static final int PT_UNIT_CIRCLE = 1;
	public static final int PT_GRAPH = 0;
	
	private static final DecimalFormat df0 = new DecimalFormat("#,##0");
	private static final DecimalFormat df1 = new DecimalFormat("#,##0.0");
	private static final DecimalFormat df2 = new DecimalFormat("#,##0.00");
	
	double fs = 500;

	@Getter
	@Setter
	protected String title = null;
	@Getter
	@Setter
	protected String y_lab = "Y_LAB_B";
	@Getter
	@Setter
	protected String x_lab = null;
	@Getter
	@Setter
	protected int xAxisType = AXIS_LINEAR;
	@Getter
	@Setter
	protected int yAxisType = AXIS_LINEAR;
	@Getter
	@Setter
	private int plotType = PT_GRAPH;

	Color cAxisMinor = Color.black;
	Color cAxis = Color.black;
	protected boolean isDefined = false;

	protected FilterInterface filter;
	protected Path2D path = new Path2D.Double();
	protected Path2D path2 = new Path2D.Double();
	protected int leftMargin = 140;
	protected int topMargin = 20;
	protected int graphHeight = 300;
	protected int graphWidth = 800;
	private double[] xAxisTicks;
	private double[] yAxisTicks;
	
	@Setter
	protected double xMin = 5.0;
	@Setter
	protected double xMax = 15000.0;

	@Setter
	protected double yMin = 10;
	@Setter
	protected double yMax = -100.0;

	// optimal tick-step calculation method
	XWilkinson wilkinson = new XWilkinson(new NiceStepSizeGenerator());

	static {
		//
	}

	public BaseCanvas() {
		setBackground(Color.WHITE);
		setBorder(new LineBorder(Color.BLACK));

		addComponentListener(this);
	}

	protected boolean isNaN(float x) {
		return x != x;
	}

	protected boolean isNaN(double x) {
		return x != x;
	}

	public void setXAxisTicks(double[] ticks) {
		xAxisTicks = ticks;
	}

	public void setYAxisTicks(double[] ticks) {
		yAxisTicks = ticks;
	}

	int yToScreen(float y) {
		AffineTransform t = calcTransform();
		Float p = new Point2D.Float(0, y);
		t.transform(p, p);
		return (int) p.y;
	}

	// protected void drawText(Graphics2D g2, double x, int y, String txt) {
	// g2.drawString(txt, (int) x, y);
	// }

	protected void drawTitle(Graphics g) {
		if (title != null) {
			g.setColor(Color.blue);
			int w = this.getWidth();
			int str_width = g.getFontMetrics().stringWidth(title);
			g.drawString(title, w - str_width - 6, 15);
		}

	}

	private void drawYLabel(Graphics2D g2, int label_gap) {
		if (y_lab != null) {
			AffineTransform t00 = g2.getTransform();

			int str_width = g2.getFontMetrics().stringWidth(y_lab);
			int tx = leftMargin - 10;
			int ty = topMargin + graphHeight / 2 + str_width / 2;

			// g2.drawString("y_lab_a", tx, ty);
			AffineTransform t0 = g2.getTransform();
			AffineTransform t1 = new AffineTransform();
			t1.translate(tx, ty);
			t1.rotate(-Math.PI / 2.0);
			t0.concatenate(t1);
			g2.setTransform(t0);

			g2.drawString(y_lab, 0, -label_gap);

			g2.setTransform(t00);
		}
	}

	private void drawXLabel(Graphics2D g2, int label_gap) {
		if (x_lab != null) {
			AffineTransform t00 = g2.getTransform();

			Rectangle2D bounds = g2.getFontMetrics().getStringBounds(x_lab, g2);
			int str_width = (int) bounds.getWidth();
			int h = (int) bounds.getHeight();
			int tx = leftMargin + graphWidth / 2 - str_width / 2;
			int ty = topMargin + graphHeight + 2;

			AffineTransform t0 = g2.getTransform();
			AffineTransform t1 = new AffineTransform();
			t1.translate(tx, ty);
			t0.concatenate(t1);
			g2.setTransform(t0);

			g2.drawString(x_lab, 0, label_gap + h);

			g2.setTransform(t00);
		}
	}

	private int drawXAxisTicks(Graphics2D g2) {
		int gap = 0;
		if (xAxisTicks == null) {
			wilkinson.setLooseFlag( true );
			int m = graphWidth / 100;
			m = ( m < 5 ) ? 5 : m; 
			double a = xMin * fs;  // origin_data.getX()
			double b = xMax * fs;  // origin_data.getX() + dataWidth
			
			Label labels = wilkinson.search( a, b, m);
			
			DecimalFormat df = df0;
			if (labels.step < 3.0)
				df = df1;
			if (labels.step < 0.30)
				df = df2;
			
			for (double tic_value = labels.min; tic_value <= labels.max; tic_value += labels.step) {
				int x = getXPos(tic_value / fs);
				String s = df.format( tic_value );
				Rectangle2D bounds = g2.getFontMetrics().getStringBounds(s, g2);
				int w = (int) bounds.getWidth();
				int h = (int) bounds.getHeight();
				if (h > gap)
					gap = h;

				g2.drawString(s, x - w / 2, topMargin + graphHeight + h + 2);
			}
		} else {
			for (double tick : xAxisTicks) {
				int x = getXPos(tick);
				String s = "" + tick;
				Rectangle2D bounds = g2.getFontMetrics().getStringBounds(s, g2);
				int w = (int) bounds.getWidth();
				int h = (int) bounds.getHeight();
				if (h > gap)
					gap = h;

				g2.drawString(s, x - w / 2, topMargin + graphHeight + h + 2);
			}
		}

		return gap + 4;
	}

	private int drawYAxisTicks(Graphics2D g2) {
		int gap = 0;
		if (yAxisTicks != null) {
		    float[] dash2 = { 4f, 4f, 4f };

		    BasicStroke bs2 = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND, 1.0f, dash2,2f);

		    for (double tick : yAxisTicks) {
				int y = getYPos(tick);
				if( y < topMargin ) {
					continue;
				}
				if( y > topMargin + graphHeight ) {
					continue;
				}
				String s = "" + tick;
				Rectangle2D bounds = g2.getFontMetrics().getStringBounds(s, g2);
				int w = (int) bounds.getWidth();
				int h = (int) bounds.getHeight();
				if (w > gap)
					gap = w;

				g2.setColor( Color.BLACK );
				g2.drawString(s, leftMargin - w - 2, topMargin + y - h/2 - 2);
				int x = (int)leftMargin + 1;
				
				g2.setColor( Color.LIGHT_GRAY);
			    g2.setStroke(bs2);
				g2.drawLine( x, y, x + graphWidth - 2, y);
			}
		}

		return gap + 4;
	}

	protected int getXPos(double tick) {
		if (xAxisType == AXIS_LINEAR) {
			double dx = (tick - xMin) / (xMax - xMin);
			int x_pos = (int) (graphWidth * dx);
			return x_pos + leftMargin;
		}
		if (xAxisType == AXIS_LOGARITHMIC) {
			double x_min = Math.log10(xMin);
			double x_max = Math.log10(xMax);
			double x_tick = Math.log10(tick);
			double dx = (x_tick - x_min) / (x_max - x_min);

			int x_pos = (int) (graphWidth * dx);
			return x_pos + leftMargin;
		}
		return 0;
	}

	protected int getYPos(double tick) {
		if (yAxisType == AXIS_LINEAR) {
			double dx = (tick - yMin) / (yMax - yMin);
			int y_pos = (int) (graphHeight * dx);
			return y_pos + topMargin;
		}
		if (yAxisType == AXIS_LOGARITHMIC) {
			double y_min = Math.log10(yMin);
			double y_max = Math.log10(yMax);
			double y_tick = Math.log10(tick);
			double dx = (y_tick - y_min) / (y_max - y_min);

			int y_pos = (int) (graphHeight * dx);
			return y_pos + leftMargin;
		}
		return 0;
	}

	@Override
	public void paint(Graphics g) {
		paint(g, true);
	}

	public void paint(Graphics g, boolean clear) {
		if (clear) {
			super.paint(g); // background and border
		}
		Graphics2D g2 = (Graphics2D) g;
		g2.setFont(new Font("default", Font.BOLD, 16));
		Stroke defaultStroke = g2.getStroke();

		drawTitle(g);

		int x1 = leftMargin;
		int y1 = topMargin;
		int x2 = leftMargin + graphWidth;
		int y2 = topMargin + graphHeight;
		int x_gap = drawXAxisTicks(g2);  // gap above
		int y_gap = drawYAxisTicks(g2);  // gap right
		
		
		g.setColor( Color.black );
		drawYLabel(g2, y_gap);
		drawXLabel(g2, x_gap);

		g2.setStroke( defaultStroke );
		g.drawLine(x1, y1, x1, y2);
		g.drawLine(x1, y2, x2, y2);
		g.drawLine(x2, y2, x2, y1);
		g.drawLine(x2, y1, x1, y1);
		g.setClip(x1, y1, graphWidth, graphHeight);
		
		if (plotType == PT_GRAPH) {

			AffineTransform t00 = g2.getTransform();
			AffineTransform t0 = g2.getTransform();
			AffineTransform t1 = calcTransform();
			if( Math.abs(t1.getScaleY()) < 0.001 ) {
				t1 = calcTransform();  // debug
			}

			double scale_x = graphWidth / (double) getWidth();
			// 053119 duplicate scaling for magnitude canvas...dont know about others
			// 053119 double scale_y = graphHeight / (double) getHeight();  
			// 053119 setting scale_y == 1.0
			AffineTransform t2 = new AffineTransform(scale_x, 0, 0, 1, x1, 0);

			t2.concatenate(t1);
			t0.concatenate(t2);
			g2.setTransform(t0);
			g2.setColor(Color.blue);

			float sx = (float) t0.getScaleX();
			float sy = (float) t0.getScaleY();
			g2.setStroke(new BasicStroke(0.15f / Math.abs(sy))); // 0.01f) );
			// System.out.println("transform t0=" + t0 + " t2=" + t2 + " " + " t1=" + t1);
			 
			g2.draw(path);
			
			g2.setColor(Color.red);
			g2.draw(path2);

			g2.setTransform(t00);
		} else if (plotType == BaseCanvas.PT_UNIT_CIRCLE) {

		}

	}

	protected AffineTransform calcTransform() {
		return new AffineTransform();
	}

	public void update() {
	}

	@Override
	public void componentResized(ComponentEvent e) {
		topMargin = 20;
		int bottomMargin = 60;
		
		if (plotType == PT_GRAPH) {
			leftMargin = 140;
			int rightMargin = 20;
			graphWidth = this.getWidth() - leftMargin - rightMargin;
			topMargin = 20;
			graphHeight = this.getHeight() - topMargin - bottomMargin;
		} else if( plotType == PT_UNIT_CIRCLE ) {
			int size = Math.min( getWidth(), getHeight() ) - topMargin - bottomMargin;
			
			graphWidth = size;
			graphHeight = size;
			leftMargin = (getWidth() - size)/2;
		}
		
		update();

	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		FilterInterface filter = (FilterInterface) evt.getNewValue();
		this.filter = filter;

		this.fs = filter.getSample_rate();
		update();
	}

}
