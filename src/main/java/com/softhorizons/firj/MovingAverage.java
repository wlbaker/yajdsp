package com.softhorizons.firj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.math3.complex.Complex;

import uk.me.berndporr.api.FilterInterface;
import uk.me.berndporr.api.PoleZeroPair;

/*
License: MIT License (http://www.opensource.org/licenses/mit-license.php)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

/* (C) 2013 Graeme Hattan & Bernd Porr */
/* (C) 2018 Bernd Porr */
/* (C) 2019 William L Baker */

public class MovingAverage implements FilterInterface {
	double[] coefficients;
	double[] buffer;
	int taps;
	int offset;
	double mu;
	private String description;
	private String name;
	private double fs;

// give the filter an array of doubles for the coefficients
	public MovingAverage(double[] _coefficients) {
		taps = _coefficients.length;
		coefficients = new double[taps]; // (new double[number_of_taps]),
		buffer = new double[taps]; // (new double[number_of_taps]()),

		coefficients = Arrays.copyOf( _coefficients, _coefficients.length );
	}

// init all coefficients and the buffer to zero
	public MovingAverage(int number_of_taps) {
		taps = number_of_taps;
		
		zeroCoeff();
		reset();
	}

// one coefficient per line
	public MovingAverage(File coeffFile) throws IOException {
		//taps = number_of_taps;
		buffer = null;
		coefficients = null;

		taps = 0;
		try (BufferedReader reader = new BufferedReader(new FileReader(coeffFile))) {

			double [] tmp_coefficients = new double[3000];
			for (;;) {
				String line = reader.readLine();
				if( line == null ) break;
				tmp_coefficients[taps] = Double.parseDouble(line);
				taps++;
			}
			coefficients = Arrays.copyOf( tmp_coefficients, taps);
			buffer = new double[taps];
		}

		if (taps == 0)
			throw new IOException("The filter has no coefficients. nTaps = 0.");

		assert (buffer != null);
		assert (coefficients != null);

		reset();
	}

	public void dispose() {
		buffer = null;
		coefficients = null;
	}

	public void lms_update(double error) {
		int buf_idx = offset;

		int idx = 0;
		while (buf_idx >= 0) {
			coefficients[idx] += buffer[idx] * error * mu;
			offset--;
		}

		buf_idx = taps - 1;

		while (idx < coefficients.length) {
			coefficients[idx] += buffer[buf_idx--] * error * mu;
			idx++;
		}
	}

	public double getTapInputPower() {

		double p = 0;

		for (int i = 0; i < taps; i++) {
			double buf_val = buffer[i];
			p += (buf_val) * (buf_val);
		}

		return p;
	}

	public double filter(double input) {
		int coeff_idx = 0; // = coefficients;
		int coeff_end = taps;

		int buf_idx = offset;

		buffer[buf_idx] = input;
		double output_ = 0;

		while (buf_idx >= 0)
			output_ += buffer[buf_idx--] * coefficients[coeff_idx++];

		buf_idx = taps - 1;

		while (coeff_idx < coeff_end)
			output_ += buffer[buf_idx--] * coefficients[coeff_idx++];

		if (++offset >= taps)
			offset = 0;

		return output_;
	}

	public void reset() {
		buffer = new double[taps]; // automatically set to 0
		offset = 0;
	}

	public void zeroCoeff() {
		coefficients = new double[taps];
		double v = 1.0 / taps;
		for( int i = 0; i < taps ; i++ ) {
			coefficients[i] = v;
		}
		offset = 0;
	}

	public int getTaps() {
		return taps;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setSample_rate(double fs) {
		this.fs = fs;
	}
	@Override
	public double getSample_rate() {
		return fs;
	}

	@Override
	public Complex response(double f) {
		return new Complex(1,0);
	}

	@Override
	public Collection<? extends PoleZeroPair> getPoleZeros() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * FIR filter interface 
	 */
	public int getNumTaps() {
		return taps;
	}

	public double getTap(int i) {
		return coefficients[i];
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
}
