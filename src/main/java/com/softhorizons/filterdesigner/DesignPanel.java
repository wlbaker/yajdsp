/*
 * Created by JFormDesigner on Thu Feb 28 09:11:05 CST 2019
 */

package com.softhorizons.filterdesigner;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;

import javax.swing.*;
import javax.swing.border.*;
import net.miginfocom.swing.*;
import uk.me.berndporr.api.Cascade;
import uk.me.berndporr.api.FilterInterface;
import uk.me.berndporr.iirj.Bessel;
import uk.me.berndporr.iirj.Butterworth;
import uk.me.berndporr.iirj.ChebyshevI;
import uk.me.berndporr.iirj.ChebyshevII;

import org.jdesktop.beansbinding.*;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;

import com.softhorizons.firj.MovingAverage;

/**
 * @author WILLIAM BAKER
 */
public class DesignPanel extends JPanel {
	ItemListener icl = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent event) {
			if (event.getStateChange() == ItemEvent.SELECTED) {
				Object item = event.getItem();
				// update graphs!
				notifyFilterListeners();
			}
		}
	};

	PropertyChangeListener pcl = new PropertyChangeListener() {

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			String text = evt.getNewValue() != null ? evt.getNewValue().toString() : "";
			System.out.println("update graph -- tf! " + text);
			notifyFilterListeners();
		}
	};

	public DesignPanel() {
		initComponents();

		foOrder.setFormatterFactory(GuiUtil.getIntegerFormatterFactory());

		rippleTF.setFormatterFactory(GuiUtil.getNumberFormatterFactory());
		fsTF.setFormatterFactory(GuiUtil.getNumberFormatterFactory());
		fstop1TF.setFormatterFactory(GuiUtil.getNumberFormatterFactory());
		fpass1TF.setFormatterFactory(GuiUtil.getNumberFormatterFactory());

		foOrder.setValue(3);
		rippleTF.setValue(0.15);
		fsTF.setValue(500.0);
		fstop1TF.setValue(80.0);
		fpass1TF.setValue(50.0);

		addSubitemChangeListener(this);

		notifyFilterListeners();
	}

	private void addSubitemChangeListener(JPanel parent) {

		Component[] comps = parent.getComponents();
		for (Component comp : comps) {
			if (comp instanceof JRadioButton) {
				JRadioButton rb = (JRadioButton) comp;
				rb.addItemListener(icl);
			} else if (comp instanceof JComboBox) {
				JComboBox cb = (JComboBox) comp;
				cb.addItemListener(icl);
			} else if (comp instanceof JFormattedTextField) {
				JFormattedTextField tf = (JFormattedTextField) comp;
				tf.addPropertyChangeListener("value", pcl);
			} else if (comp instanceof JPanel) {
				addSubitemChangeListener((JPanel) comp); // recurse
			}
		}
	}

	public void notifyFilterListeners() {
		FilterInterface filter = null;

		System.out.println("notify action");

		String sOrder = foOrder.getText();
		int order;
		try {
			order = Integer.parseInt(sOrder);
		} catch (Exception ex) {
			// invalid format
			foPanel.setForeground(Color.red);
			return;
		}
		foPanel.setForeground(null);

		Object filterName;
		if (dmIIR.isSelected()) {
			if (lowpassRB.isSelected()) {
				fstop1TF.setEnabled(true);
				fpass1TF.setEnabled(false);
			} else if (highpassRB.isSelected()) {
				fstop1TF.setEnabled(true);
				fpass1TF.setEnabled(false);
			} else if (bandpassRB.isSelected()) {
				fstop1TF.setEnabled(true);
				fpass1TF.setEnabled(true);
			}
			foSpecifyOrder.setText("Specify order");
			filterName = dmIIRFilterType.getSelectedItem();
		} else if (dmFIR.isSelected()) {
			fstop1TF.setEnabled(true);
			fpass1TF.setEnabled(false);
			rippleTF.setEnabled(false);
			lowpassRB.setSelected(true);
			lowpassRB.setEnabled(false);
			highpassRB.setEnabled(false);
			bandpassRB.setEnabled(false);
			bandstopRB.setEnabled(false);
			diffRB.setEnabled(false);
			foSpecifyOrder.setText("Specify taps");
			filterName = dmFIRFilterType.getSelectedItem();
		} else {
			filterName = null;
			// invalid
			System.err.println("Invalid filter selection: null");
		}

		Number fs = (Number) fsTF.getValue();

		StringBuilder description = new StringBuilder();
		description.append("{");
		appendPair(description, "name", filterName);
		appendPair(description, "order", order);
		appendPair(description, "fs", fs);

		if ("Chebyshev I".equals(filterName) || "Chebyshev II".equals(filterName)) {
			rippleTF.setVisible(true);
			rippleTF.setEnabled(true);
		} else {
			rippleTF.setVisible(true);
			rippleTF.setEnabled(false);
		}

		if ("Bessel".equals(filterName)) {
			Bessel bessel = new Bessel();

			if (lowpassRB.isSelected()) {
				Number cutoff = (Number) fstop1TF.getValue();
				bessel.lowPass(order, fs.doubleValue(), cutoff.doubleValue());
				appendPair(description, "type", "lowpass");
				appendPair(description, "cutoff", cutoff);
			} else if (highpassRB.isSelected()) {
				Number cutoff = (Number) fstop1TF.getValue();
				bessel.highPass(order, fs.doubleValue(), cutoff.doubleValue());
				appendPair(description, "type", "highpass");
				appendPair(description, "cutoff", cutoff);
			} else if (bandpassRB.isSelected()) {
				Number pass = (Number) fpass1TF.getValue();
				Number cutoff = (Number) fstop1TF.getValue();
				double width = cutoff.doubleValue() - pass.doubleValue();
				double center = (cutoff.doubleValue() + pass.doubleValue()) / 2;
				if (width < 0) {
					width = -width;
				}
				bessel.bandPass(order, fs.doubleValue(), center, width);
				appendPair(description, "type", "bandpass");
				appendPair(description, "center", center);
				appendPair(description, "width", width);
			} else if (bandstopRB.isSelected()) {

				Number pass = (Number) fpass1TF.getValue();
				Number cutoff = (Number) fstop1TF.getValue();
				double width = cutoff.doubleValue() - pass.doubleValue();
				double center = (cutoff.doubleValue() + pass.doubleValue()) / 2;
				if (width < 0) {
					width = -width;
				}
				bessel.bandStop(order, fs.doubleValue(), center, width);
				appendPair(description, "type", "bandstop");
				appendPair(description, "center", center);
				appendPair(description, "width", width);
			} else {
				rtPanel.setForeground(Color.red);
				return;
			}
			rtPanel.setForeground(null);
			filter = bessel;
		} else if ("Butterworth".equals(filterName)) {
			Butterworth bw = new Butterworth();

			if (lowpassRB.isSelected()) {
				Number cutoff = (Number) fstop1TF.getValue();
				bw.lowPass(order, fs.doubleValue(), cutoff.doubleValue());
				appendPair(description, "type", "lowpass");
				appendPair(description, "cutoff", cutoff);
			} else if (highpassRB.isSelected()) {
				Number cutoff = (Number) fstop1TF.getValue();
				bw.highPass(order, fs.doubleValue(), cutoff.doubleValue());
				appendPair(description, "type", "highpass");
				appendPair(description, "cutoff", cutoff);
			} else if (bandpassRB.isSelected()) {
				Number pass = (Number) fpass1TF.getValue();
				Number cutoff = (Number) fstop1TF.getValue();
				double width = cutoff.doubleValue() - pass.doubleValue();
				double center = (cutoff.doubleValue() + pass.doubleValue()) / 2;
				if (width < 0) {
					width = -width;
				}
				bw.bandPass(order, fs.doubleValue(), center, width);
				appendPair(description, "type", "bandpass");
				appendPair(description, "center", center);
				appendPair(description, "width", width);
			} else if (bandstopRB.isSelected()) {
				Number pass = (Number) fpass1TF.getValue();
				Number cutoff = (Number) fstop1TF.getValue();
				double width = cutoff.doubleValue() - pass.doubleValue();
				double center = (cutoff.doubleValue() + pass.doubleValue()) / 2;
				if (width < 0) {
					width = -width;
				}
				bw.bandStop(order, fs.doubleValue(), center, width);
				appendPair(description, "type", "bandstop");
				appendPair(description, "center", center);
				appendPair(description, "width", width);
			} else {
				rtPanel.setForeground(Color.red);
				return;
			}
			rtPanel.setForeground(null);
			filter = bw;
		} else if ("Chebyshev I".equals(filterName)) {
			ChebyshevI cheby = new ChebyshevI();

			Number rippleDb = (Number) rippleTF.getValue();
			if (lowpassRB.isSelected()) {
				Number cutoff = (Number) fstop1TF.getValue();
				cheby.lowPass(order, fs.doubleValue(), cutoff.doubleValue(), rippleDb.doubleValue());
				appendPair(description, "type", "lowpass");
				appendPair(description, "cutoff", cutoff);
			} else if (highpassRB.isSelected()) {
				Number cutoff = (Number) fstop1TF.getValue();
				cheby.highPass(order, fs.doubleValue(), cutoff.doubleValue(), rippleDb.doubleValue());
				appendPair(description, "type", "highpass");
				appendPair(description, "cutoff", cutoff);
			} else if (bandpassRB.isSelected()) {
				Number pass = (Number) fpass1TF.getValue();
				Number cutoff = (Number) fstop1TF.getValue();
				double width = cutoff.doubleValue() - pass.doubleValue();
				double center = (cutoff.doubleValue() + pass.doubleValue()) / 2;
				if (width < 0) {
					width = -width;
				}
				rippleTF.setVisible(true);
				rippleTF.setEnabled(true);
				cheby.bandPass(order, fs.doubleValue(), center, width, rippleDb.doubleValue());
				appendPair(description, "type", "bandpass");
				appendPair(description, "center", center);
				appendPair(description, "width", width);
			} else if (bandstopRB.isSelected()) {
				Number pass = (Number) fpass1TF.getValue();
				Number cutoff = (Number) fstop1TF.getValue();
				double width = cutoff.doubleValue() - pass.doubleValue();
				double center = (cutoff.doubleValue() + pass.doubleValue()) / 2;
				if (width < 0) {
					width = -width;
				}
				rippleTF.setVisible(true);
				rippleTF.setEnabled(true);
				cheby.bandStop(order, fs.doubleValue(), center, width, rippleDb.doubleValue());
				appendPair(description, "type", "bandstop");
				appendPair(description, "center", center);
				appendPair(description, "width", width);
			} else {
				rtPanel.setForeground(Color.red);
				return;
			}
			appendPair(description, "ripple", rippleDb.doubleValue());
			rtPanel.setForeground(null);
			filter = cheby;
		} else if ("Chebyshev II".equals(filterName)) {
			ChebyshevII cheby = new ChebyshevII();

			Number rippleDb = (Number) rippleTF.getValue();

			if (lowpassRB.isSelected()) {
				Number cutoff = (Number) fstop1TF.getValue();
				cheby.lowPass(order, fs.doubleValue(), cutoff.doubleValue(), rippleDb.doubleValue());
				appendPair(description, "type", "lowpass");
				appendPair(description, "cutoff", cutoff);
			} else if (highpassRB.isSelected()) {
				Number cutoff = (Number) fstop1TF.getValue();
				cheby.highPass(order, fs.doubleValue(), cutoff.doubleValue(), rippleDb.doubleValue());
				appendPair(description, "type", "highpass");
				appendPair(description, "cutoff", cutoff);
			} else if (bandpassRB.isSelected()) {
				Number pass = (Number) fpass1TF.getValue();
				Number cutoff = (Number) fstop1TF.getValue();
				double width = cutoff.doubleValue() - pass.doubleValue();
				double center = (cutoff.doubleValue() + pass.doubleValue()) / 2;
				if (width < 0) {
					width = -width;
				}
				cheby.bandPass(order, fs.doubleValue(), center, width, rippleDb.doubleValue());
				appendPair(description, "type", "bandpass");
				appendPair(description, "center", center);
				appendPair(description, "width", width);
			} else if (bandstopRB.isSelected()) {
				Number pass = (Number) fpass1TF.getValue();
				Number cutoff = (Number) fstop1TF.getValue();
				double width = cutoff.doubleValue() - pass.doubleValue();
				double center = (cutoff.doubleValue() + pass.doubleValue()) / 2;
				if (width < 0) {
					width = -width;
				}
				cheby.bandStop(order, fs.doubleValue(), center, width, rippleDb.doubleValue());
				appendPair(description, "type", "bandstop");
				appendPair(description, "center", center);
				appendPair(description, "width", width);
			} else {
				rtPanel.setForeground(Color.red);
				return;
			}
			appendPair(description, "ripple", rippleDb.doubleValue());

			rtPanel.setForeground(null);
			filter = cheby;
		} else if ("Equiripple".equals(filterName)) {
			MovingAverage ma = new MovingAverage(order);
			System.err.println("FIXME: implement equiripple filter");
			filter = ma;
		} else if ("Elliptic".equals(filterName)) {
			MovingAverage ma = new MovingAverage(order);
			System.err.println("FIXME: implement elliptic filter");
			filter = ma;
		} else if ("Moving Average".equals(filterName)) {
			MovingAverage ma = new MovingAverage(order);
			filter = ma;
		}

		description.append("}");
		filter.setSample_rate(fs.doubleValue());
		filter.setDescription(description.toString());

		this.putClientProperty("Filter", filter);
	}

	private void appendPair(StringBuilder dest, String name, Object value) {
		if (dest.length() > 4) {
			dest.append(",");
		}
		dest.append("\"");
		dest.append(name);
		dest.append("\":");
		if (value instanceof Number) {
			dest.append(value);
		} else {
			dest.append("\"");
			dest.append(value);
			dest.append("\"");
		}

	}

	private void dmIIRActionPerformed(ActionEvent e) {
		foSpecifyOrder.setText("Specify order");
	}

	private void dmFIRActionPerformed(ActionEvent e) {
		foSpecifyOrder.setText("Specify taps");
	}

	private void dmFIRFilterTypeItemStateChanged(ItemEvent e) {
		System.out.println("item state changed: " + e);
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents
		panel1 = new JPanel();
		rtPanel = new JPanel();
		lowpassRB = new JRadioButton();
		highpassRB = new JRadioButton();
		bandpassRB = new JRadioButton();
		bandstopRB = new JRadioButton();
		diffRB = new JRadioButton();
		panel3 = new JPanel();
		dmIIR = new JRadioButton();
		dmIIRFilterType = new JComboBox<>();
		dmFIR = new JRadioButton();
		dmFIRFilterType = new JComboBox<>();
		panel4 = new JPanel();
		foPanel = new JPanel();
		foSpecifyOrder = new JRadioButton();
		foOrder = new JFormattedTextField();
		foMinimumOrder = new JRadioButton();
		panel7 = new JPanel();
		label2 = new JLabel();
		comboBox2 = new JComboBox<>();
		label3 = new JLabel();
		fsTF = new JFormattedTextField();
		label4 = new JLabel();
		fstop1TF = new JFormattedTextField();
		label5 = new JLabel();
		fpass1TF = new JFormattedTextField();
		label12 = new JLabel();
		rippleTF = new JFormattedTextField();

		// ======== this ========
		setLayout(new MigLayout("hidemode 3",
				// columns
				"[grow,fill]" + "[grow,fill]",
				// rows
				"[grow,fill]"));

		// ======== panel1 ========
		{
			panel1.setLayout(new MigLayout("insets 0,hidemode 3",
					// columns
					"[grow,fill]",
					// rows
					"[]" + "[grow,fill]"));

			// ======== rtPanel ========
			{
				rtPanel.setBorder(new TitledBorder("Response Type"));
				rtPanel.setLayout(new MigLayout("insets 0,hidemode 3",
						// columns
						"[fill]",
						// rows
						"[]" + "[]" + "[]" + "[]" + "[]"));

				// ---- lowpassRB ----
				lowpassRB.setText("Lowpass");
				lowpassRB.setSelected(true);
				rtPanel.add(lowpassRB, "cell 0 0");

				// ---- highpassRB ----
				highpassRB.setText("Highpass");
				rtPanel.add(highpassRB, "cell 0 1");

				// ---- bandpassRB ----
				bandpassRB.setText("Bandpass");
				rtPanel.add(bandpassRB, "cell 0 2");

				// ---- bandstopRB ----
				bandstopRB.setText("Bandstop");
				rtPanel.add(bandstopRB, "cell 0 3");

				// ---- diffRB ----
				diffRB.setText("Differentiator");
				rtPanel.add(diffRB, "cell 0 4");
			}
			panel1.add(rtPanel, "cell 0 0");

			// ======== panel3 ========
			{
				panel3.setBorder(new TitledBorder("Design Method"));
				panel3.setLayout(new MigLayout("hidemode 3",
						// columns
						"[fill]" + "[grow,fill]",
						// rows
						"[]" + "[]" + "[grow,fill]"));

				// ---- dmIIR ----
				dmIIR.setText("IIR");
				dmIIR.setSelected(true);
				dmIIR.addActionListener(e -> dmIIRActionPerformed(e));
				panel3.add(dmIIR, "cell 0 0");

				// ---- dmIIRFilterType ----
				dmIIRFilterType.setModel(new DefaultComboBoxModel<>(
						new String[] { "Bessel", "Butterworth", "Chebyshev I", "Chebyshev II" }));
				panel3.add(dmIIRFilterType, "cell 1 0");

				// ---- dmFIR ----
				dmFIR.setText("FIR");
				dmFIR.addActionListener(e -> dmFIRActionPerformed(e));
				panel3.add(dmFIR, "cell 0 1");

				// ---- dmFIRFilterType ----
				dmFIRFilterType.setModel(
						new DefaultComboBoxModel<>(new String[] { "Equiripple", "Elliptic", "Moving Average" }));
				dmFIRFilterType.addItemListener(e -> dmFIRFilterTypeItemStateChanged(e));
				panel3.add(dmFIRFilterType, "cell 1 1");
			}
			panel1.add(panel3, "cell 0 1");
		}
		add(panel1, "cell 0 0");

		// ======== panel4 ========
		{
			panel4.setLayout(new MigLayout("insets 0,hidemode 3",
					// columns
					"[grow,fill]",
					// rows
					"[]" + "[grow,fill]"));

			// ======== foPanel ========
			{
				foPanel.setBorder(new TitledBorder("Filter Order"));
				foPanel.setLayout(new MigLayout("hidemode 3",
						// columns
						"[fill]" + "[50:n,fill]",
						// rows
						"[]" + "[]"));

				// ---- foSpecifyOrder ----
				foSpecifyOrder.setText("Specify order");
				foSpecifyOrder.setSelected(true);
				foPanel.add(foSpecifyOrder, "cell 0 0");

				// ---- foOrder ----
				foOrder.setText("6");
				foPanel.add(foOrder, "cell 1 0");

				// ---- foMinimumOrder ----
				foMinimumOrder.setText("Minimum order");
				foMinimumOrder.setEnabled(false);
				foPanel.add(foMinimumOrder, "cell 0 1");
			}
			panel4.add(foPanel, "cell 0 0");

			// ======== panel7 ========
			{
				panel7.setBorder(new TitledBorder("Frequency Specifications"));
				panel7.setLayout(new MigLayout("hidemode 3",
						// columns
						"[grow,right]" + "[50:n,fill]",
						// rows
						"[]" + "[]" + "[]" + "[]" + "[]" + "[]" + "[grow]"));

				// ---- label2 ----
				label2.setText("Units:");
				panel7.add(label2, "cell 0 0");

				// ---- comboBox2 ----
				comboBox2.setModel(new DefaultComboBoxModel<>(new String[] { "Hz" }));
				panel7.add(comboBox2, "cell 1 0");

				// ---- label3 ----
				label3.setText("Fs:");
				panel7.add(label3, "cell 0 1");
				panel7.add(fsTF, "cell 1 1");

				// ---- label4 ----
				label4.setText("Cutoff 1:");
				panel7.add(label4, "cell 0 2");
				panel7.add(fstop1TF, "cell 1 2");

				// ---- label5 ----
				label5.setText("Cutoff 2:");
				panel7.add(label5, "cell 0 3");
				panel7.add(fpass1TF, "cell 1 3");

				// ---- label12 ----
				label12.setText("Ripple:");
				panel7.add(label12, "cell 0 4");

				// ---- rippleTF ----
				rippleTF.setText("0.15");
				panel7.add(rippleTF, "cell 1 4");
			}
			panel4.add(panel7, "cell 0 1");
		}
		add(panel4, "cell 1 0");

		// ---- rtButtonGroup ----
		ButtonGroup rtButtonGroup = new ButtonGroup();
		rtButtonGroup.add(lowpassRB);
		rtButtonGroup.add(highpassRB);
		rtButtonGroup.add(bandpassRB);
		rtButtonGroup.add(bandstopRB);
		rtButtonGroup.add(diffRB);

		// ---- dmButtonGroup ----
		ButtonGroup dmButtonGroup = new ButtonGroup();
		dmButtonGroup.add(dmIIR);
		dmButtonGroup.add(dmFIR);

		// ---- foButtonGroup ----
		ButtonGroup foButtonGroup = new ButtonGroup();
		foButtonGroup.add(foSpecifyOrder);
		foButtonGroup.add(foMinimumOrder);

		// ---- bindings ----
		bindingGroup = new BindingGroup();
		bindingGroup.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ, dmIIR, BeanProperty.create("enabled"),
				dmIIRFilterType, BeanProperty.create("enabled")));
		bindingGroup.addBinding(Bindings.createAutoBinding(UpdateStrategy.READ, dmFIR, BeanProperty.create("enabled"),
				dmFIRFilterType, BeanProperty.create("enabled")));
		bindingGroup.bind();
		// JFormDesigner - End of component initialization //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
	private JPanel panel1;
	private JPanel rtPanel;
	private JRadioButton lowpassRB;
	private JRadioButton highpassRB;
	private JRadioButton bandpassRB;
	private JRadioButton bandstopRB;
	private JRadioButton diffRB;
	private JPanel panel3;
	private JRadioButton dmIIR;
	private JComboBox<String> dmIIRFilterType;
	private JRadioButton dmFIR;
	private JComboBox<String> dmFIRFilterType;
	private JPanel panel4;
	private JPanel foPanel;
	private JRadioButton foSpecifyOrder;
	private JFormattedTextField foOrder;
	private JRadioButton foMinimumOrder;
	private JPanel panel7;
	private JLabel label2;
	private JComboBox<String> comboBox2;
	private JLabel label3;
	private JFormattedTextField fsTF;
	private JLabel label4;
	private JFormattedTextField fstop1TF;
	private JLabel label5;
	private JFormattedTextField fpass1TF;
	private JLabel label12;
	private JFormattedTextField rippleTF;
	private BindingGroup bindingGroup;
	// JFormDesigner - End of variables declaration //GEN-END:variables
}
