package com.softhorizons.filterdesigner;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.KeyboardFocusManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatterFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.NumberFormatter;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;
import com.jtattoo.plaf.graphite.GraphiteLookAndFeel;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class GuiUtil {

	private static boolean isFullScreen;

	private static final String ENTER_ACTION_NAME = "ddd_enter_action";

	private static DefaultFormatterFactory currencyFactory;

	private static DefaultFormatterFactory numberFactory;

	private static DefaultFormatterFactory integerFactory;
	private static Window root_window;

	// private static GraphicsDevice device;

	public static void initApplication(String appName, String[] args) throws UnsupportedLookAndFeelException {
		initApplication(appName, args, true);
	}

	public static void initApplication(String appName, String[] args, boolean lookAndFeel)
			throws UnsupportedLookAndFeelException {
		// PrefUtil.setAppName(appName);
		if (lookAndFeel) {
			setLookAndFeel();
		}

		//initGlobalHotkeys();

	}

	public static Action getEnterAction(JComponent c) {
		return (Action) c.getClientProperty(ENTER_ACTION_NAME);
	}

	/**
	 * Redirect an application's System.out and Sytem.err to a log file.  The location can be either
	 * a file or a directory.  If the location is a directory, a log file name is created using the application
	 * name and timestamp info.
	 * 
	 * @param applicationName The application name, used to create a log file name when location is a directory.
	 * @param location		  Destination file/directory for log.
	 */
	public static void redirectStdOutAndErr(String applicationName, String location) {
		try {
			Date date = Calendar.getInstance().getTime();

			String outName = location;

			File f = new File(location);
			if (f.isDirectory()) {
				Format formatter = new SimpleDateFormat("yyyyMMdd");
				String now = "_" + formatter.format(date);
				formatter = new SimpleDateFormat("HHmmss");
				now += "_" + formatter.format(date);
				outName = location + applicationName + now + ".log";
			}

			try {
				File p = f.getParentFile();
				if (p.exists() == false) {
					p.mkdirs();
				}
			} catch (Exception e) {
				// ignore
			}

			System.setOut(new PrintStream(new FileOutputStream(outName)));
			System.setErr(System.out);
		} catch (FileNotFoundException fnfe) {
			System.out.println(fnfe.getMessage());
		}
	}

	public static void initScreen(GraphicsDevice device, JFrame f, boolean fullScreen) {

		if (device == null) {
			GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice[] devices = env.getScreenDevices();

			device = devices[0];
		}

		isFullScreen = false;
		if (device != null && fullScreen) {
			isFullScreen = device.isFullScreenSupported();
		}

		if (f.isDisplayable()) {
			// release all the native resources before setting undecorated
			f.dispose();
		}

		// substance has an issue with setUndecorated...?
		//
		// f.getT
		// if( decorateFrame ) {
		// f.setUndecorated(isFullScreen);
		// }
		// f.setResizable(!isFullScreen);

		if (isFullScreen) {
			f.setVisible(false);
			// Full-screen mode
			device.setFullScreenWindow(f);
			f.validate();
		} else {
			// Windowed mode
			f.pack();
			f.setVisible(true);
		}

		root_window = f;

	}

	/**
	 * 
	 * Best reference on L&F
	 * https://stackoverflow.com/questions/3954616/java-look-and-feel-lf/28753722#28753722
	 */
		
	private static void setLookAndFeel() throws UnsupportedLookAndFeelException {

		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);

	  try {
		// UIManager.setLookAndFeel("de.javasoft.plaf.synthetica.SyntheticaStandardLookAndFeel");
		//UIManager.setLookAndFeel("de.javasoft.plaf.synthetica.SyntheticaBlackEyeLookAndFeel");
		// UIManager.setLookAndFeel("de.javasoft.plaf.synthetica.SyntheticaClassyLookAndFeel");
		  
		// UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
		// GraphiteLookAndFeel.setTheme("Graphite", "", "");
		Properties props = new Properties();
		props.put("logoString", "");
		GraphiteLookAndFeel.setCurrentTheme(props);
		UIManager.setLookAndFeel("com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
		return;
	} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  /* -- begin substance L&F
		  //Another way is to use the #setLookAndFeel method of the SyntheticaLookAndFeel class
		  //SyntheticaLookAndFeel.setLookAndFeel(String className, boolean antiAlias, boolean useScreenMenuOnMac);
		UIManager.setLookAndFeel(new SubstanceRavenLookAndFeel() );
		boolean ok = SubstanceLookAndFeel.setSkin(new org.pushingpixels.substance.api.skin.RavenSkin());
		// SubstanceCortex.GlobalScope.setSkin(new org.pushingpixels.substance.api.skin.RavenSkin());
		
		if( !ok ) {
			// throw new UnsupportedLookAndFeelException("could not load RavenSkin");
			log.error("throw new UnsupportedLookAndFeelException(\"could not load RavenSkin ??\");");
		}
		if (!SubstanceLookAndFeel.isCurrentLookAndFeel()) {
			log.error("throw new UnsupportedLookAndFeelException(\"could not install Substance L&F\");" );
		}
		-- end substancd L&F */
		// SubstanceLookAndFeel.setSkin(new
		// org.pushingpixels.substance.api.skin.CremeCoffeeSkin() );
		// UIManager.put("ComboBox.background", Color.RED );
		// UIManager.put("ComboBox.foreground", Color.WHITE );
		// UIManager.put("ComboBox.buttonBackground", Color.YELLOW );
		// UIManager.put("ComboBox.buttonDarkShadow", Color.WHITE );
		// UIManager.put("ComboBox.buttonHighlight", Color.YELLOW );
		// UIManager.put("ComboBox.buttonShadow", Color.YELLOW );
		// UIManager.put("ComboBox.control", Color.YELLOW );
		// UIManager.put("ComboBox.controlForeground", Color.CYAN );
		// UIManager.put("ComboBox.controlBackground", Color.MAGENTA );
		// UIManager.put("ComboBox.disabledForeground", Color.WHITE );
		// UIManager.put("ComboBox.selectionBackground", Color.RED );
		// UIManager.put("ComboBox.selectionForeground", Color.WHITE );

		/*
		 * NimRODLookAndFeel laf = new com.nilo.plaf.nimrod.NimRODLookAndFeel();
		 * 
		 * String themeName = "/nimrod/darkgrey.theme"; try { java.net.URL url =
		 * GuiUtil.class.getResource(themeName); NimRODTheme theme = new
		 * NimRODTheme(url); NimRODLookAndFeel.setCurrentTheme(theme); } catch
		 * (Exception e) { System.err.println("could not load default theme: " +
		 * themeName); }
		 * 
		 * UIManager.setLookAndFeel(laf);
		 */

		/*
		 * tattoo try { UIManager.setLookAndFeel(
		 * "com.jtattoo.plaf.graphite.GraphiteLookAndFeel"); } catch
		 * (ClassNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (InstantiationException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } catch
		 * (IllegalAccessException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
		// // UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
		// //
		// UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");
		// // UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
		// //
		// UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

	}


	private static File chosen = null;
	private static DefaultFormatterFactory datetimeFactory;
	private static DefaultFormatterFactory hhmmssFactory;


	public static void showError(final String prefix, final Throwable e1) {
		showError(root_window, prefix, e1);
	}

	public static void showError(final Component parent, final String prefix, final Throwable e1) {
		e1.printStackTrace();

		if ("Setting shape for full-screen window is not supported.".equals(e1.getMessage())) {
			// let the user ignore...
			return;
		}

		Runnable r = new Runnable() {

			@Override
			public void run() {

				String message = e1.getMessage();
				if (message == null) {
					message = e1.getClass().getName();
				}
				String summary = prefix + ": " + message;

				ExceptionDialogPanel ld = new ExceptionDialogPanel(summary, e1);
				if (isFullScreen) {
					ld.setSize(600, 300);
					ld.setMinimumSize(new Dimension(600, 300));
					ld.setPreferredSize(new Dimension(600, 300));
					ld.doLayout();
				}

				GuiUtil.doDialog("Application Error", parent, ld);
			}

		};

		if (SwingUtilities.isEventDispatchThread())

		{
			r.run();
		} else

		{
			try {
				SwingUtilities.invokeAndWait(r);
			} catch (Exception e) {
				// Not really much to do except give up if you can't tell the
				// user there is a problem.
				e.printStackTrace();
			}
		}

	}

	public static void doDialog(String title, Component parent, JPanel panel) {


			Object[] options = new Object[] {};

			JOptionPane op = new JOptionPane(panel, JOptionPane.PLAIN_MESSAGE, JOptionPane.NO_OPTION, null, options);

			JDialog dlg = op.createDialog(parent, title);
			//GuiUtil.initModalDialog(dlg, panel);
			// dlg.setModalityType( Dialog.ModalityType.APPLICATION_MODAL );
			// //dlg.setUndecorated( true );
			dlg.setResizable(true);

			Window prev_root = root_window;
			try {
				root_window = dlg;
				dlg.setVisible(true);
			} finally {
				root_window = prev_root;
			}

	
	}


	public static void showErrorInGuiThread(final String prefix, final Throwable e) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				showError(prefix, e);

			}
		});
	}

	/*
	 * This routine provides application level setup of a user panel. All JPanel
	 * constructors should call this routine to provide consistent look-n-feel
	 * kinds of setup on the panel.
	 */

	public static String getDateTimeFormatString() {
		return "MM/dd/yyyy HH:mm:ss";
	}

	public static DateFormat getDateTimeFormat() {
		DateFormat datef = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		return datef;
	}

	public static DateFormat getDateFormat() {
		DateFormat datef = new SimpleDateFormat("MM/dd/yyyy");
		return datef;
	}

	public static DateFormat getTimeFormat() {
		DateFormat datef = new SimpleDateFormat("HH:mm:ss");
		return datef;
	}

	public static void showMessage(final String message) {
		showMessage( message, true );
	}
	
	public static void showMessage(final String message, final boolean modal ) {

		Runnable r = new Runnable() {

			@Override
			public void run() {
				String m = message;
				if (m.startsWith("<!DOCTYPE html")) {
					int beginIndex = m.indexOf("<html", 10);
					if (beginIndex > 0) {
						m = m.substring(beginIndex);
					}
					// clip the DOCTYP&DTD
				}
				if (m.startsWith("<html ")) {  
					int beginIndex = m.indexOf(">", 5);
					if (beginIndex > 0) {
						m = "<html>" + m.substring(beginIndex);
					}
					// cut out namespace stuff
				}
				Object o = m;
				// special handling for html?
				if (m.startsWith("<!DOCTYPE html") || m.startsWith("<html")) {
					HTMLEditorKit kit = new HTMLEditorKit();

					// add some styles to the html
//					StyleSheet styleSheet = kit.getStyleSheet();
//					styleSheet.addRule("body {color:#000; font-family:times; margin: 4px; }");
//					styleSheet.addRule("h1 {color: blue;}");
//					styleSheet.addRule("h2 {color: #ff0000;}");
//					styleSheet.addRule("pre {font : 10px monaco; color : black; background-color : #fafafa; }");

					Document doc = kit.createDefaultDocument();

					JEditorPane ed1=new JEditorPane();
					ed1.setEditorKit( kit );
					ed1.setDocument( doc );
					ed1.setText( m );
					
					ed1.setEditable(false);
					// JLabel label = new JLabel(m);
					JScrollPane sp = new JScrollPane(ed1);
					
					sp.setPreferredSize( new Dimension(400,300));
					
					JPanel p = new JPanel();

					//======== this ========
					p.setLayout(new FormLayout(
						"[400dlu,default,600dlu]:grow",
						"fill:[200dlu,default,300dlu]:grow, $lgap, default"));
					p.add(sp, CC.xy(1, 1));
					
					o = p;
					JOptionPane op = new JOptionPane(p, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_OPTION);
					JDialog d = op.createDialog(root_window, "Message");
					//GuiUtil.initModalDialog(d, op); // makes the escape key work
					d.setModal( modal );
					d.setResizable(true);
					d.pack();
					d.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(root_window, o);
				}
			}

		};

		if (SwingUtilities.isEventDispatchThread()) {
			r.run();
		} else {
			try {
				SwingUtilities.invokeAndWait(r);
			} catch (Exception e) {
				// Not really much to do except give up if you can't tell the
				// user there is a problem.
				e.printStackTrace();
			}
		}

	}

	public static int showConfirmDialog(Object message, String title, int option) {
		return JOptionPane.showConfirmDialog(null, message, title, option);
	}

	public static void selectComboString(JComboBox printerList, String value) {
		for (int idx = 0; idx < printerList.getItemCount(); idx++) {
			Object o = printerList.getItemAt(idx);
			if (value.equals(o.toString())) {
				printerList.setSelectedIndex(idx);
				break;
			}
		}

	}

	public static AbstractFormatterFactory getDateTimeFormatterFactory() {
		if (datetimeFactory == null) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			DateFormatter df = new DateFormatter(dateFormat);

			df.setAllowsInvalid(false);

			datetimeFactory = new DefaultFormatterFactory(df, // default
					df, // display
					df // edit
			);
		}
		return datetimeFactory;
	}

	public static AbstractFormatterFactory getHHMMSSFormatterFactory() {
		if (hhmmssFactory == null) {
			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			DateFormatter df = new DateFormatter(dateFormat);

			df.setAllowsInvalid(false);

			hhmmssFactory = new DefaultFormatterFactory(df, // default
					df, // display
					df // edit
			);
		}
		return hhmmssFactory;
	}

	// not sure this is the best place for it, but it needs to be somewhere

	public static AbstractFormatterFactory getCurrencyFormatterFactory() {
		if (currencyFactory == null) {
			DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();

			NumberFormatter currencyDisplayFormatter = new NumberFormatter(currencyFormat);

			DecimalFormat currencyEditFormat = new DecimalFormat("#0.00");
			currencyEditFormat.setParseBigDecimal(true);

			NumberFormatter currencyEditFormatter = new NumberFormatter(currencyEditFormat);

			currencyEditFormatter.setAllowsInvalid(true);

			currencyFactory = new DefaultFormatterFactory(currencyDisplayFormatter, // default
					currencyDisplayFormatter, // display
					currencyEditFormatter // edit
			);
		}

		return currencyFactory;
	}

	public static AbstractFormatterFactory getNumberFormatterFactory() {
		if (numberFactory == null) {
			DecimalFormat numFormat = new DecimalFormat("###,##0.0###");
			NumberFormatter num1 = new NumberFormatter(numFormat);
			numFormat.setParseBigDecimal(true);

			DecimalFormat numEditFormat = new DecimalFormat("#0.0###");
			numEditFormat.setParseBigDecimal(true);

			NumberFormatter numEdit = new NumberFormatter(numEditFormat);

			num1.setAllowsInvalid(false);
			// ?numEdit.setMaximum(new Double(30.0));
			numEdit.setAllowsInvalid(true);

			numberFactory = new DefaultFormatterFactory(num1, // default
					num1, // display
					numEdit // edit
			);
		}

		return numberFactory;
	}

	public static AbstractFormatterFactory getIntegerFormatterFactory() {
		if (integerFactory == null) {
			DecimalFormat numFormat = new DecimalFormat("#,##0");
			NumberFormatter num1 = new NumberFormatter(numFormat);
			numFormat.setParseIntegerOnly(true);

			DecimalFormat numEditFormat = new DecimalFormat("#0");
			numEditFormat.setParseIntegerOnly(true);

			NumberFormatter numEdit = new NumberFormatter(numEditFormat);
			numEdit.setValueClass(Integer.class);

			num1.setAllowsInvalid(false);
			numEdit.setAllowsInvalid(true);

			integerFactory = new DefaultFormatterFactory(num1, // default
					num1, // display
					numEdit // edit
			);
		}

		return integerFactory;
	}

	/*
	 * used as an object for scripts
	 */

	public static GuiUtil getInstance() {
		return new GuiUtil();
	}

	public static Color getColorFromName(String color) {
		Color ccolor = Color.MAGENTA;
		if ("red".equalsIgnoreCase(color)) {
			ccolor = Color.RED;
		} else if ("green".equalsIgnoreCase(color)) {
			ccolor = Color.GREEN;
		} else if ("blue".equalsIgnoreCase(color)) {
			ccolor = Color.BLUE;
		} else if ("black".equalsIgnoreCase(color)) {
			ccolor = Color.BLACK;
		} else if ("gray".equalsIgnoreCase(color)) {
			ccolor = Color.GRAY;
		} else if ("orange".equalsIgnoreCase(color)) {
			ccolor = Color.ORANGE;
		}
		return ccolor;
	}

	public static String[] parseArgs(String[] args) {

		List<String> remaining = new LinkedList<String>();
		
		LogManager.getRootLogger().setLevel((Level) Level.ERROR);
		for (int i = 0; i < args.length; i++) {

			if ("-log".equals(args[i]) || "--log".equals(args[i])) {
				i++;
				String fileName = args[i];
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
				Date now = new Date();
				String str = df.format(now);

				if (fileName.indexOf("{Date}") > 0) {
					fileName = fileName.replaceAll("\\{Date\\}", str);
				}
				if (fileName.indexOf("{DATE}") > 0) {
					fileName = fileName.replaceAll("\\{DATE\\}", str);
				}
				GuiUtil.redirectStdOutAndErr("mgDaq", fileName);
				System.out.println("Logging started at " + str);
			} else if ("-d".equals(args[i])) {
				String loggerName = args[++i];
				Logger logger = Logger.getLogger(loggerName);
				if (logger == null) {
					log.error("NO SUCH LOGGER for DEBUG: {}", loggerName);
				} else {
					System.out.println("setting log level: " + loggerName + " to ALL");
					logger.setLevel(Level.ALL);
				}
			} else if ("-w".equals(args[i])) {
				String loggerName = args[++i];
				Logger logger = Logger.getLogger(loggerName);
				if (logger == null) {
					log.error("NO SUCH LOGGER for DEBUG: {}", loggerName);
				} else {
					System.out.println("setting log level: " + loggerName + " to WARN");
					logger.setLevel(Level.WARN);
				}
			} else if ("-i".equals(args[i])) {
				String loggerName = args[++i];
				Logger logger = Logger.getLogger(loggerName);
				if (logger == null) {
					log.error("NO SUCH LOGGER for DEBUG: {}", loggerName);
				} else {
					System.out.println("setting log level: " + loggerName + " to INFO");
					logger.setLevel(Level.INFO);
				}
			} else if ("-ii".equals(args[i])) {
				LogManager.getRootLogger().setLevel((Level) Level.INFO);
			} else if ("-ww".equals(args[i])) {
				LogManager.getRootLogger().setLevel((Level) Level.WARN);
			} else if ("-dd".equals(args[i])) {
				LogManager.getRootLogger().setLevel((Level) Level.DEBUG);
			} else if ("-cap".equals(args[i])) {
				System.setProperty("devicelog_dir", args[++i]);
			} else if ("-jars".equals(args[i])) {
				 ClassLoader cl = ClassLoader.getSystemClassLoader();

				 if( cl instanceof URLClassLoader) { 
					 URL[] urls = ((URLClassLoader)cl).getURLs();

					 for(URL url: urls){
						 System.out.println(url.getFile());
					 }
				 } else {
					 System.out.println("JARS not available in this classloader: " + cl.getClass().getCanonicalName() );
				 }
			} else {
				remaining.add( args[i] );
			}
		}

		Thread.setDefaultUncaughtExceptionHandler(new GuiUncaughtExceptionHandler());
		
		args = new String[ remaining.size() ];
		remaining.toArray( args );
		return args;
	}

	public static void setRootWindow(Window f) {
		root_window = f;
	}

	public static void closeDialog(JPanel panel) {

		Container parent = panel.getParent();

		while (parent != null) {
			//if (parent instanceof DisabledGlassPane)
			//	break;
			if (parent instanceof JDialog)
				break;

			parent = parent.getParent();
		}

//		if (parent instanceof DisabledGlassPane) {
//			DisabledGlassPane p0 = (DisabledGlassPane) parent;
//			p0.deactivate();
//
//			if (listener != null) {
//				listener.closeDialogAction(panel);
//			}
//		} else if (parent instanceof JDialog) {
			JDialog p0 = (JDialog) parent;
			p0.setVisible(false);

			// ERROR: This is not the right thing to do here...the dialog must
			// have
			// a window listener for close events...consider the X in the
			// corner.
//		}

	}


}
