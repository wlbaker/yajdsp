package com.softhorizons.filterdesigner;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JFrame;
import javax.swing.UnsupportedLookAndFeelException;

public class FilterDesigner {

	public static void main( String [] args ) {
		
		try {
			GuiUtil.initApplication("filterEvalTool", args);
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		EvaluatePanel ep = new EvaluatePanel();
		ep.addDesignPanel(null);
		
		JFrame f = new JFrame("Filter Evaluation");
		f.addComponentListener( new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				super.componentResized(arg0);
				ep.resizeCanvases();
			}
			
		} );
		f.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		f.add(ep);
		f.pack();
		f.setSize( 1024, 800);
		f.setVisible(true);
	}


}
