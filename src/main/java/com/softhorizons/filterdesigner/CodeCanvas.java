/*
 * Created by JFormDesigner on Mon Mar 11 11:03:02 CDT 2019
 */

package com.softhorizons.filterdesigner;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.*;

import com.softhorizons.firj.MovingAverage;

import net.miginfocom.swing.*;
import uk.me.berndporr.api.Biquad;
import uk.me.berndporr.api.Cascade;
import uk.me.berndporr.api.FilterInterface;

/**
 * @author WILLIAM BAKER
 */
public class CodeCanvas extends JPanel implements PropertyChangeListener {
	private FilterInterface filter;

	public CodeCanvas() {
		initComponents();
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents
		scrollPane1 = new JScrollPane();
		textArea1 = new JTextArea();
		panel1 = new JPanel();

		// ======== this ========
		setLayout(new MigLayout("hidemode 3",
				// columns
				"[grow,fill]",
				// rows
				"[grow,fill]" + "[]"));

		// ======== scrollPane1 ========
		{
			scrollPane1.setViewportView(textArea1);
		}
		add(scrollPane1, "cell 0 0");

		// ======== panel1 ========
		{
			panel1.setLayout(new MigLayout("hidemode 3",
					// columns
					"[fill]" + "[fill]" + "[fill]",
					// rows
					"[]"));
		}
		add(panel1, "cell 0 1");
		// JFormDesigner - End of component initialization //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
	private JScrollPane scrollPane1;
	private JTextArea textArea1;
	private JPanel panel1;
	// JFormDesigner - End of variables declaration //GEN-END:variables

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		FilterInterface filter = (FilterInterface) evt.getNewValue();
		this.filter = filter;

		update();
	}

	public void update() {
		StringBuffer t = new StringBuffer();
		if (filter != null) {
			if (filter instanceof Cascade) {
				Cascade cascade = (Cascade) filter;
				for (int i = 0; i < cascade.getNumBiquads(); i++) {
					t.append("BIQUAD[" + i + "] = { \n");
					Biquad biquad = cascade.getBiquad(i);
					t.append("\tB0 = " + biquad.getB0() + "\n");
					t.append("\tB1 = " + biquad.getB1() + "\n");
					t.append("\tB2 = " + biquad.getB2() + "\n");
					t.append("\tA0 = " + biquad.getA0() + "\n");
					t.append("\tA1 = " + biquad.getA1() + "\n");
					t.append("\tA2 = " + biquad.getA2() + "\n");
					t.append("}\n\n");
				}
			// } else if (filter instanceof MultiTap ) {
			} else if (filter instanceof MovingAverage ) {
				MovingAverage ma = (MovingAverage) filter;
				for (int i = 0; i < ma.getNumTaps(); i++) {
					t.append("\ttap[" + i + "] = " + ma.getTap( i ) + "\n");
				}
			}
		}
		textArea1.setText(t.toString());
	}
}
