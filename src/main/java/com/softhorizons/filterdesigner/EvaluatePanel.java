/*
 * Created by JFormDesigner on Thu Feb 28 10:33:48 CST 2019
 */

package com.softhorizons.filterdesigner;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.*;

import com.softhorizons.yajdsps.BaseCanvas;
import com.softhorizons.yajdsps.GroupCanvas;
import com.softhorizons.yajdsps.MagnitudeCanvas;
import com.softhorizons.yajdsps.PhaseCanvas;
import com.softhorizons.yajdsps.PoleZeroCanvas;
import com.softhorizons.yajdsps.StepCanvas;

/**
 * @author WILLIAM BAKER
 */
public class EvaluatePanel extends JPanel {
	private DesignPanel design;
	private MagnitudeCanvas magnitude;
	private PhaseCanvas phaser;
	private GroupCanvas grouper;
	private StepCanvas stepper;
	private PoleZeroCanvas polezero;
	private CodeCanvas coder;
	private JTabbedPane tabber;

	public EvaluatePanel() {
		initComponents();
		magnitude = new MagnitudeCanvas();
		phaser = new PhaseCanvas();
		grouper = new GroupCanvas();
		stepper = new StepCanvas();
		polezero = new PoleZeroCanvas();
		coder = new CodeCanvas();

		tabber = new JTabbedPane();
		tabber.addTab("Magnitude", magnitude);
		tabber.addTab("Phase", phaser);
		tabber.addTab("Group Delay", grouper);
		tabber.addTab("Step Response", stepper);
		tabber.addTab("Pole/Zero", polezero);
		tabber.addTab("Code", coder);

		this.add(tabber, BorderLayout.CENTER);
	}

	public DesignPanel getDesignPanel() {
		return design;
	}

	public void setDesignPanel(DesignPanel designer) {
		this.design = designer;

		if (designer != null) {
			designer.addPropertyChangeListener("Filter", magnitude);
			designer.addPropertyChangeListener("Filter", phaser);
			designer.addPropertyChangeListener("Filter", grouper);
			designer.addPropertyChangeListener("Filter", stepper);
			designer.addPropertyChangeListener("Filter", polezero);
			designer.addPropertyChangeListener("Filter", coder);
			
			designer.notifyFilterListeners();
		}
	}

	public void addDesignPanel(DesignPanel design) {
		if (design == null) {
			design = new DesignPanel();
		}
		this.add(design, BorderLayout.SOUTH);

		setDesignPanel(design);
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY
		// //GEN-BEGIN:initComponents

		// ======== this ========
		setLayout(new BorderLayout());
		// JFormDesigner - End of component initialization //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
	// JFormDesigner - End of variables declaration //GEN-END:variables

	protected void resizeCanvases() {
		for (int i = 0; i < tabber.getTabCount(); i++) {
			Component comp = tabber.getTabComponentAt(i);
			BaseCanvas canvas = (BaseCanvas) comp;
			if (canvas != null) { // this happens on startup
				canvas.componentResized(null);
			}
		}

	}
}
