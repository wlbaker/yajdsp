package com.softhorizons.yajdspfx;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * ZetCode JavaFX tutorial
 *
 * This program draws three lines which form a rectangle.
 * 
 * Author: Jan Bodnar Website: zetcode.com Last modified: June 2015
 */

public class ZerosChart extends Application {

	@Override
	public void start(Stage stage) {

		initUI(stage);
	}

	private void initUI(Stage stage) {

		Pane root = new Pane();

		Canvas canvas = new Canvas(300, 300);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		drawLines(gc);

		root.getChildren().add(canvas);

		Scene scene = new Scene(root, 300, 300, Color.WHITESMOKE);

		stage.setTitle("Lines");
		stage.setScene(scene);
		stage.show();

		ChangeListener<Number> stageSizeListener = new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
					GraphicsContext gc = canvas.getGraphicsContext2D();
					gc.clearRect(0, 0, 300, 300);
					drawLines(gc);
			} }
		;

		stage.widthProperty().addListener(stageSizeListener);
		stage.heightProperty().addListener(stageSizeListener);
	}

	private void drawLines(GraphicsContext gc) {

		double w = gc.getCanvas().getWidth();
		double h = gc.getCanvas().getHeight();
		w = Math.min(w, h);

		System.out.println("w=" + w + " --- h=" + h);

		gc.beginPath();
		gc.strokeOval(20, 20, w - 40, w - 40);
		gc.moveTo(30.5, 30.5);
		gc.lineTo(150.5, 30.5);
		gc.lineTo(150.5, 150.5);
		gc.lineTo(30.5, 30.5);
		gc.stroke();
	}

	public static void main(String[] args) {
		launch(args);
	}
}