package com.softhorizons.yajdspfx;

import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.Gauge.SkinType;
import eu.hansolo.medusa.GaugeBuilder;
import eu.hansolo.medusa.skins.SlimSkin;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
//import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Main extends Application {  
    private GridPane pane;  
    private Gauge    steps;  
    private Gauge    freq;  
    private Gauge    ripple;  
    private Gauge    foodCalories;  
    private Gauge    weight;  
    private Gauge    bodyFat;  
 
    static {
   	 System.setProperty("user.home", "c:/tmp");
   	 System.out.println("user.home=" + System.getProperty("user.home") );
    }
    
     @Override public void init() {  
        GaugeBuilder builder = GaugeBuilder.create().skinType( SkinType.SLIM ).prefWidth(50).prefHeight(50);
        
        steps          = builder.decimals(0).maxValue(10000).unit("STEPS").build();  
        freq       = builder.decimals(2).maxValue(10).unit("Hz").build();  
        ripple = builder.decimals(0).maxValue(2200).unit("dB").build();  
        foodCalories   = builder.decimals(0).maxValue(2200).unit("KCAL").build();  
        weight         = builder.decimals(1).maxValue(85).unit("KG").build();  
        bodyFat        = builder.decimals(1).maxValue(20).unit("%").build();  
 
         VBox stepsBox        = getGuageBox("Order", Color.rgb(77,208,225), steps);  
        VBox distanceBox     = getGuageBox("Freq", Color.rgb(255,183,77), freq);  
        //VBox foodCaloriesBox = getTopicBox("Ripple", Color.rgb(129,199,132), ripple );  
        VBox rippleBox = getGuageBox("Ripple", Color.rgb(229,115,115),  
                                             ripple);  
 
          pane = new GridPane();  
        pane.setPadding(new Insets(20));  
        pane.setHgap(10);  
        pane.setVgap(15);  
        pane.setBackground(new Background(new BackgroundFill(Color.rgb(39,44,50), CornerRadii.EMPTY, Insets.EMPTY)));  
        pane.add(stepsBox, 0, 0);  
        pane.add(distanceBox, 1, 0);  
        pane.add(rippleBox, 2, 0);  
        //pane.add(foodCaloriesBox, 0, 1);  
 }  
 
@Override public void start(Stage stage) {  
        Scene scene = new Scene(pane);  
 
        steps.setValue(5201);  
        freq.setValue(3.12);  
        ripple.setValue(347);  
        foodCalories.setValue(1500);  
        weight.setValue(78.7);  
        bodyFat.setValue(14.2);  
 
        stage.setTitle("Medusa Dashboard");  
        stage.setScene(scene);  
        stage.show();  
    }  
 
@Override public void stop() { System.exit(0); }  
 
private VBox getGuageBox(final String TEXT, final Color COLOR, final Gauge GAUGE) {  
        Rectangle bar = new Rectangle(100, 3);  
        bar.setArcWidth(6);  
        bar.setArcHeight(6);  
        bar.setFill(COLOR);  
 
        Label label = new Label(TEXT);  
        label.setTextFill(COLOR);  
        label.setAlignment(Pos.CENTER);  
        label.setPadding(new Insets(0, 0, 10, 0));  
 
        GAUGE.setBarColor(COLOR);  
      //  GAUGE.setBarBackgroundColor(Color.rgb(39,44,50);  
        GAUGE.setAnimated(true);  
 
        VBox vBox = new VBox(bar, label, GAUGE);  
        vBox.setSpacing(3);  
        vBox.setAlignment(Pos.CENTER);  
        return vBox;  
    }  
 
     public static void main(String[] args) { 
    	 launch(args); }   
}  